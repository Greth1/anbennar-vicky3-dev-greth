﻿#Anbennar - probably need to make a private company deal here, so this is used by VG, DTC and CLSTC. Vanilla has chartered company so just use that tbh
gov_guild = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_TITLE_GUILDMASTER"
	female_ruler = "RULER_TITLE_GUILDMASTER"
	
	possible = {
		exists = c:B05
		c:B05 = ROOT
		has_law = law_type:law_presidential_republic
		country_has_voting_franchise = no
		NOT = { is_subject_type = subject_type_dominion }
		NOT = { is_subject_type = subject_type_puppet }
	}
}

gov_company = {
	transfer_of_power = presidential_elective
	new_leader_on_reform_government = no

	male_ruler = "RULER_TITLE_DIRECTOR"
	female_ruler = "RULER_TITLE_DIRECTOR"
	
	possible = {
		has_law = law_type:law_presidential_republic
		OR = {
			AND = {
				exists = c:B03 
				c:B03 = ROOT
			}
		}
		NOT = { is_subject_type = subject_type_dominion }
		NOT = { is_subject_type = subject_type_puppet }
	}
}