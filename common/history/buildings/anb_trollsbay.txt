﻿BUILDINGS={
	s:STATE_CESTIRMARK={
		region_state:B19={
			create_building={
				building="building_glassworks"
				level=2
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_disabled_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
			#Cause they administer the Trollsbay fed
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
	}
	s:STATE_YANEKIN={
		region_state:B19={
			create_building={
				building="building_logging_camp"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
		}
	}
	s:STATE_TROLLSBRIDGE={
		region_state:B19={
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
		}
	}
	s:STATE_MARLLIANDE={
		region_state:B20={
			create_building={
				building="building_cotton_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
	}
	s:STATE_BUSHBANKS={
		region_state:B20={
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
		}
	}
	s:STATE_YNNSMOUTH={
		region_state:B21={
			create_building={
				building="building_maize_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_religious_academia" }
			}
			create_building={
				building="building_textile_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
		}
	}
	s:STATE_ZANLIB={
		REGION_STATE:B22={
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_no_luxuries" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
	}
	s:STATE_ISOBELIN={
		REGION_STATE:B23={
			create_building={
				building="building_furniture_manufacturies"
				level=2
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_no_luxuries" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_cotton_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
	}
	s:STATE_BOEK={
		REGION_STATE:B23={
			create_building={
				building="building_logging_camp"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
		}
		REGION_STATE:B24={
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}
	s:STATE_THILVIS={
		REGION_STATE:B24={
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_maize_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_food_industry"
				level=2
				reserves=1
				activate_production_methods={ "pm_bakery" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
	}
	s:STATE_VALORPOINT={
		region_state:B25={
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
		region_state:B26={
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
	}
	s:STATE_OKHIBOLI={
		region_state:B26={
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
	}
	s:STATE_SPOORLAND={
		region_state:B26={
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned" }
			}
		}
	}
}