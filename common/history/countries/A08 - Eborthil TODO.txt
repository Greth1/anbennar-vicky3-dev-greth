﻿COUNTRIES = {
	c:A08 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_national_guard

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_protectionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_colonial_resettlement
		# No Police
		activate_law = law_type:law_charitable_health_system
		# No Schools

		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_all_races_allowed

		ig:ig_intelligentsia = {
			add_ruling_interest_group = yes
		}
		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}
		ig:ig_petty_bourgeoisie = {
			add_ruling_interest_group = yes
		}
		ig:ig_devout = {
			set_interest_group_name = ig_ravelian_church
			remove_ideology = ideology_moralist
			add_ideology = ideology_ravelian_moralist
			remove_ideology = ideology_pious
			add_ideology = ideology_scholarly
		}
	}
}