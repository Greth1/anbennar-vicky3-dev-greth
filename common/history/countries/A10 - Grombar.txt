﻿COUNTRIES = {
	c:A10 = {
		effect_starting_technology_tier_2_tech = yes
		
		# Laws 
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy 
		activate_law = law_type:law_national_supremacy # needed so that e.g. Poles are discriminated against
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_colonial_resettlement
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_women_own_property # Not allowed women in workplace without voting
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_traditional_magic_encouraged
		
		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
		ig:ig_devout = {
			set_interest_group_name = ig_corinite_faithful
			remove_ideology = ideology_moralist
			add_ideology = ideology_corinite_moralist
			remove_ideology = ideology_patriarchal
			add_ideology = ideology_feminist_ig
			add_ideology = ideology_anti_slavery
		}
	}
}