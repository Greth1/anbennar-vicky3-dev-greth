﻿COUNTRIES = {
	c:A20 = {
		effect_starting_technology_tier_3_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation	#they should accept farrani etc so whatever does that
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats	#as per elven long life and ship stuff, best pick the best person, elected not allowed without voting
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism	#exporting elven goods to Cannor
		activate_law = law_type:law_land_based_taxation	#elven long life prob taught to tax all not just landowner. BUT Per-capita not allowed under traditionalism!
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_traditional_magic_only

		ig:ig_landowners = {
			remove_ideology = ideology_paternalistic
			add_ideology = ideology_elven_havenist
		}
		ig:ig_devout = {
			set_interest_group_name = ig_adeanic_temples
			remove_ideology = ideology_moralist
			add_ideology = ideology_adeanic_moralist
		}
	}
}