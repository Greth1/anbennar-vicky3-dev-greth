﻿COUNTRIES = {
	c:A36 = {
		effect_starting_technology_tier_1_tech = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia

		activate_law = law_type:law_laissez_faire
		activate_law = law_type:law_free_trade
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_colonial_exploitation
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		
	}
}