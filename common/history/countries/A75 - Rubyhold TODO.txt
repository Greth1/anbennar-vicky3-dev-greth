﻿COUNTRIES = {
	c:A75 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_colonial_resettlement
		activate_law = law_type:law_local_police
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_censorship
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slavery_banned


	}
}