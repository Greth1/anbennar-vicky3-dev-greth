﻿COUNTRIES = {
	c:C38 = {
		effect_starting_technology_tier_5_tech = yes
		
		activate_law = law_type:law_cheifdom
		activate_law = law_type:law_elder_council
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats		
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_no_home_affairs
		activate_law = law_type:law_protectionism
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_consumption_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_slave_trade
		
		activate_law = law_type:law_same_race_and_humans

		#ig:ig_devout = {
		#	add_ruling_interest_group = yes
		#}
	}
}