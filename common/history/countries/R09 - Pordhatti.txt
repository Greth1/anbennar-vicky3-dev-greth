﻿COUNTRIES = {
	c:R09 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy #Until stratocracy is created
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production
	}
}