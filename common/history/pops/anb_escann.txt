﻿POPS = {
	s:STATE_NORTH_IBEVAR= {
		region_state:A20 = {
			create_pop = {
				culture = moon_elven
				size = 703001
				split_religion = {
					moon_elven = {
						elven_forebears = 0.25
						regent_court = 0.75
					}
				}
			}
		}
	}

	s:STATE_SOUTH_IBEVAR= {
		region_state:A20 = {
			create_pop = {
				culture = moon_elven
				size = 555000
				split_religion = {
					moon_elven = {
						elven_forebears = 0.25
						regent_court = 0.75
					}
				}
			}
		}
	}

	s:STATE_CURSEWOOD= {
		region_state:A20 = {
			create_pop = {
				culture = moon_elven
				size = 201030
				split_religion = {
					moon_elven = {
						elven_forebears = 0.25
						regent_court = 0.75
					}
				}
			}
			create_pop = {
				culture = luciander
				size = 375010
			}
		}
	}

	s:STATE_FARRANEAN= {
		region_state:A20 = {
			create_pop = {
				culture = farrani
				size = 1150954
			}
			create_pop = {
				culture = luciander
				size = 140601
			}
			create_pop = {
				culture = moon_elven
				size = 100000
				split_religion = {
					moon_elven = {
						elven_forebears = 0.25
						regent_court = 0.75
					}
				}
			}
		}
		region_state:A22 = {	#Ancardian
			create_pop = {
				culture = farrani
				size = 210503
			}
			create_pop = {
				culture = ancardian
				size = 163626
			}
			create_pop = {
				culture = moon_elven
				size = 2000
			}
		}
	}

	s:STATE_ADENICA= {
		region_state:A22 = {
			create_pop = {
				culture = ancardian
				size = 1300500
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 10643
			}
		}
	}

	s:STATE_ROHIBON= {
		region_state:A22 = {
			create_pop = {
				culture = ancardian
				size = 100550
			}
			create_pop = {
				culture = elikhander
				size = 1150503
				split_religion = {
					elikhander = {
						corinite = 0.25
						khetist = 0.75
					}
				}
			}
		}
	}

	s:STATE_ANCARDIAN_PLAINS= {
		region_state:A22 = {
			create_pop = {
				culture = ancardian
				size = 1303830
			}
			create_pop = {
				culture = newfoot_halfling
				size = 98598
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 32063
			}
		}
	}

	s:STATE_MORECED= {
		region_state:A22 = {
			create_pop = {
				culture = ancardian
				size = 302030
			}
			create_pop = {
				culture = ionnic
				size = 803205
			}
			create_pop = {
				culture = newfoot_halfling
				size = 55930
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 458853
			}
		}
	}

	s:STATE_OUDMERE= {
		region_state:A25 = {
			create_pop = {
				culture = ancardian
				size = 190241
			}
			create_pop = {
				culture = ionnic
				size = 1330205
			}
			create_pop = {
				culture = marcher
				size = 250402
			}
			create_pop = {
				culture = newfoot_halfling
				size = 10930
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 965094
			}
		}
	}

	s:STATE_NEWSHIRE= {
		region_state:A24 = {
			create_pop = {
				culture = newfoot_halfling
				size = 1074050
			}
		}
	}

	s:STATE_DEVACED= {
		region_state:A27 = {
			create_pop = {
				culture = marcher
				size = 814010
			}
			create_pop = {
				culture = stalboric
				size = 670390
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 173394
			}
		}
	}

	s:STATE_VERNHAM= {
		region_state:A27 = {
			create_pop = {
				culture = stalboric
				size = 1040591
			}
			create_pop = {
				culture = marcher
				size = 430591
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 279603
			}
		}
	}
	s:STATE_DOSTANS_WAY= {
		region_state:A27 = {
			create_pop = {
				culture = marcher
				size = 1004912
			}
			create_pop = {
				culture = corvurian	#from ravenmarch
				size = 310496
			}
		}
	}

	s:STATE_BLADEMARCH= {
		region_state:A27 = {
			create_pop = {
				culture = marcher
				size = 1340340
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 240485
			}
		}
	}
	s:STATE_ROSANVORD= {
		region_state:A28 = {
			create_pop = {
				culture = rosanda
				size = 1003683
			}
			create_pop = {
				culture = ungulan_orc	#the last vestiges of orcish slavery
				size = 735830
				pop_type = slaves
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 40938
			}
		}
	}

	s:STATE_MEDIRLEIGH= {
		region_state:A28 = {
			create_pop = {
				culture = rosanda
				size = 850704
			}
			create_pop = {
				culture = ungulan_orc
				size = 700483
				pop_type = slaves
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 64338
			}
		}
	}

	s:STATE_BURNOLL= {
		region_state:A28 = {
			create_pop = {
				culture = rosanda
				size = 180602
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 110491
			}
			create_pop = {
				culture = ungulan_orc
				size = 51842
				pop_type = slaves
			}
			create_pop = {
				culture = city_goblin
				size = 90509
				pop_type = slaves
			}
		}
		region_state:A32 = {
			create_pop = {
				culture = rosanda
				size = 400700
			}
			create_pop = {
				culture = nurcestiran
				size = 220591
			}
			create_pop = {
				culture = city_goblin
				size = 246028
			}
		}
	}

	s:STATE_SILVERVORD= {
		region_state:A26 = {
			create_pop = {
				culture = heartman
				size = 973671
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 18482
			}
		}
	}

	s:STATE_CANNWOOD= {
		region_state:A26 = {
			create_pop = {
				culture = heartman
				size = 462030
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 504
			}
		}
	}

	s:STATE_EBONMARCK= {
		region_state:A10 = {
			create_pop = {
				culture = heartman
				size = 1554621
			}
			create_pop = {
				culture = grombari_half_orc
				size = 5091
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 15351
			}
		}
	}

	s:STATE_MIDDLE_ALEN= {
		region_state:A10 = {
			create_pop = {
				culture = heartman
				size = 50961
			}
			create_pop = {
				culture = gawedi
				size = 109910
			}
			create_pop = {
				culture = grombari_half_orc
				size = 279910
			}
		}
	}


	s:STATE_DRYADSDALE= {
		region_state:A29 = {
			create_pop = {
				culture = marrodic
				size = 1104941
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 67049
			}
		}
	}

	s:STATE_HORNWOOD= {
		region_state:A29 = {
			create_pop = {
				culture = marrodic
				size = 1006443
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 3049
			}
		}
	}

	s:STATE_MARRVALE= {
		region_state:A29 = {
			create_pop = {
				culture = marrodic
				size = 1104535
			}
		}
	}

	s:STATE_MARRHOLD= {
		region_state:A29 = {
			create_pop = {
				culture = marrodic
				size = 2059314 # Assume this is meant to be across all the states in the hold
			}
		}
	}

	s:STATE_ESSHYL= {
		region_state:A29 = {
			create_pop = {
				culture = marrodic
				size = 768401
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 20482
			}
			create_pop = {
				culture = ungulan_orc
				size = 50682
			}
		}
	}

	s:STATE_UNGULDAVOR= {
		region_state:A31 = {
			create_pop = {
				culture = ungulan_orc
				size = 1413015
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 120130
			}
		}
	}

	s:STATE_KONDUNN= {
		region_state:A31 = {
			create_pop = {
				culture = ungulan_orc
				size = 1104931
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 104120
			}
		}
	}

	s:STATE_CASTONATH= {
		region_state:A30 = {
			create_pop = {
				culture = nurcestiran
				size = 1844530
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 454892
			}
			create_pop = {
				culture = ungulan_orc
				size = 27498
			}
			create_pop = {
				culture = wood_elven
				size = 78394
			}
			create_pop = {
				culture = iron_dwarf
				size = 45194
			}
			create_pop = {
				culture = city_goblin
				size = 365058
			}
		}
	}

	s:STATE_UPPER_NATH= {
		region_state:A30 = {
			create_pop = {
				culture = nurcestiran
				size = 1155934
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 125334
			}
			create_pop = {
				culture = wood_elven
				size = 47380
			}
		}
	}

	s:STATE_EASTGATE= {
		region_state:A30 = {
			create_pop = {
				culture = nurcestiran
				size = 1355934
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 406394
			}
			create_pop = {
				culture = ungulan_orc
				size = 984590
			}
		}
	}

	s:STATE_STEELHYL= {
		region_state:A30 = {
			create_pop = {
				culture = nurcestiran
				size = 640395
			}
			create_pop = {
				culture = iron_dwarf
				size = 140194
			}
		}
	}

	s:STATE_SERPENTSMARCK= {
		region_state:A30 = {
			create_pop = {
				culture = nurcestiran
				size = 959383
			}
			create_pop = {
				culture = iron_dwarf
				size = 72039
			}
		}
	}

	s:STATE_NORTHYL= {
		region_state:A30 = {
			create_pop = {
				culture = nurcestiran
				size = 1005593
			}
			create_pop = {
				culture = iron_dwarf
				size = 170039
			}
			create_pop = {
				culture = agate_dwarf
				size = 45689
			}
		}
	}

	s:STATE_WESTGATE= {
		region_state:A30 = {
			create_pop = {
				culture = nurcestiran
				size = 1005925
			}
			create_pop = {
				culture = heartman	#wyvernhert refugees
				size = 330843
			}
			create_pop = {
				culture = wood_elven
				size = 5439
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 45598
			}
			create_pop = {
				culture = city_goblin
				size = 32058
			}
		}
	}

	s:STATE_LOWER_NATH= {
		region_state:A30 = {
			create_pop = {
				culture = nurcestiran
				size = 1104950
			}
			create_pop = {
				culture = cannorian_half_orc
				size = 19058
			}
		}
	}

}