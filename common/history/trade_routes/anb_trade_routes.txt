﻿TRADE_ROUTES = {
	# Anbennar
	c:A01={
		create_trade_route = {
			goods = wood
			level = 1
			direction = import
			target = c:A10.market
		}
		create_trade_route = {
			goods = iron
			level = 3
			direction = export
			target = c:A06.market
		}
	}
	
	# Northern League
	c:A04 = {
		create_trade_route = {
			goods = engines
			level = 3
			direction = import
			target = c:A06.market
		}
		create_trade_route={
			goods = steel
			level = 1
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = liquor
			level = 9
			direction = export
			target = c:A04.market
		}
	}
	
	#Gnomish Hiearchy
	c:A06 = {
		create_trade_route = {
			goods = fertilizer
			level = 6
			direction = export
			target = c:A14.market
		}
		create_trade_route = {
			goods = wood
			level = 3
			direction = import
			target = c:A14.market
		}
		create_trade_route = {
			goods = fabric
			level = 3
			direction = import
			target = c:A14.market
		}
		create_trade_route = {
			goods = tools
			level = 3
			direction = export
			target = c:A14.market
		}
	}
	
	#Small Country
	c:A14 = {
		create_trade_route = {
			goods = clippers
			level = 1
			direction = import
			target = c:A01.market
		}
		create_trade_route = {
			goods = manowars
			level = 1
			direction = import
			target = c:A01.market
		}
	}
	
	#Lorent
	c:A03 = {
		create_trade_route = {
			goods = wood
			level = 3
			direction = import
			target = c:A14.market
		}
		#This will probs get reduced once lorentish colonies are making fabric for them
		create_trade_route = {
			goods = fabric
			level = 3
			direction = import
			target = c:A14.market
		}
	}
}