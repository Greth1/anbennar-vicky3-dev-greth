﻿building_output_damestear_add = {
	good = yes
	percent = no
}

building_input_damestear_add = {
	good = no
	percent = no
}

building_output_blueblood_add = {
	good = yes
	percent = no
}

building_input_blueblood_add = {
	good = no
	percent = no
}

building_output_relics_add = {
	good = yes
	percent = no
}

building_input_relics_add = {
	good = no
	percent = no
}

building_output_mithril_add = {
	good = yes
	percent = no
}

building_input_mithril_add = {
	good = no
	percent = no
}

building_output_commercial_skyships_add = {
	good = yes
	percent = no
}

building_input_commercial_skyships_add = {
	good = no
	percent = no
}

building_output_military_skyships_add = {
	good = yes
	percent = no
}

building_input_military_skyships_add = {
	good = no
	percent = no
}

building_output_magical_reagents_add = {
	good = yes
	percent = no
}

building_input_magical_reagents_add = {
	good = no
	percent = no
}

building_output_artifice_doodads_add = {
	good = yes
	percent = no
}

building_input_artifice_doodads_add = {
	good = no
	percent = no
}

building_output_magic_add = {
	good = yes
	percent = no
}

building_input_magic_add = {
	good = no
	percent = no
}

building_input_porcelain_add = { #Not in vanilla since it is only a consumer good there
	good = no
	percent = no
}

building_damestear_mine_throughput_mult = {
	good = yes
	percent = yes
}

building_damestear_fields_throughput_mult = {
	good = yes
	percent = yes
}

building_mage_academy_throughput_mult = {
	good = yes
	percent = yes
}
