colors	=	{
	#Anbennar Colours
	# rgb { 28 124 140 }

	aelnar_blue	=	rgb { 10 27 143 }

	arannen_blue_light	=	rgb { 100 150 190 }

	busilari_orange	=	rgb { 211 85 4 } #Busilar's color in EU4

	corvurian_red_dark	=	rgb { 118 17 19 }
	corvurian_red_light	=	rgb { 161 28 47 }

	damerian_blue	=	rgb { 28 124 140 }
	damerian_blue_light	=	rgb { 39 160 163 }
	damerian_black	=	rgb { 33 33 33 }
	damerian_white	=	rgb { 239 241 242 }

	derannic_purple	=	rgb { 102 45 145 }
	derannic_pink	=	rgb { 218 28 92 }

	eborthili_gold	=	rgb { 207 182 100 } #Eborthil's color in EU4
	eborthili_gold_light	=	rgb { 237 223 131 }

	enteben_red	=	rgb { 120 40 40 }

	estalli_green	=	rgb { 158 207 111 }

	gawedi_blue	=	rgb { 48 51 108 }
	gawedi_yellow	=	rgb {209 173 103 }

	gnomish_pink	=	rgb { 212 158 197 }

	ibevar_blue	=	rgb { 210 237 246 }

	iochand_blue	=	rgb { 58 50 153 }
	iochand_green	=	rgb { 176 255 153 }

	irrliam_yellow	=	rgb { 230 200 80 }

	kheios_teal	=	rgb { 60 120 170 }

	khet_orange	=	rgb { 230 172 85 }

	lorentish_red	=	rgb { 237 28 36 }
	lorentish_red_dark	=	rgb { 164 29 33 }
	lorentish_green	=	rgb { 75 131 61 }
	lorentish_green_dark	=	rgb { 55 111 41 }
	lorentish_lilac	=	rgb { 168 128 186 }
	lorentish_gold	=	rgb { 255 239 159 }

	pearlsedge_blue	=	rgb { 46 49 146 }
	pearlsedge_pearl	=	rgb { 254 248 223 }

	roilsardi_red	=	rgb { 185 30 68 }
	roilsardi_green	=	rgb { 176 229 104 }

	small_blue	=	rgb { 30 75 125 }
	small_green	=	rgb { 10 102 49 }
	small_red	=	rgb { 183 23 31 }
	small_yellow	=	rgb { 216 191 23 }
	small_orange	=	rgb { 214 86 32 }
	small_purple_light	=	rgb { 143 85 163 }
	small_purple_dark	=	rgb { 77 42 124 }

	vanburian_red	=	rgb { 132 38 39 }
	vanburian_grey	=	rgb { 167 169 172 }
	
	verne_red	=	rgb { 150 0 0 } #Verne's color in EU4
	verne_wyvern_red	=	rgb { 125 49 40 }
	verne_beige	=	rgb { 194 181 155 }

	wexonard_purple	=	rgb { 97 0 137 }
	
	beige	=	rgb { 155 129 108 }
	beige_light	=	rgb { 190 160 135 }
	brown_dark	=	rgb { 40 24 15}
	cream	=	rgb { 224 213 175 }
	lime	=	rgb { 150 180 80 }
	mud	=	rgb { 60 70 0 }
	purpure	=	rgb { 133 23 69 }
	pink	=	rgb { 220 120 140 }
	pink_light	=	rgb { 250 180 210 }
	sapphire	=	rgb { 50 70 140 }
	silver	=	rgb { 172 180 196 }
	turquoise	=	rgb { 34 165 178 }
} 