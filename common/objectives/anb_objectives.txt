﻿anb_objective_realize_the_dream = {
	texture = "gfx/interface/icons/placeholder_icons/objective.png"
	
	recommended_tags = { A01 A10 }
	
	objective_subgoals = {
		sg_encourage_liberal_ideas
		sg_pass_laws
		sg_public_services
		sg_liberate_the_slaves
		sg_women_and_children
		sg_egalitarian_society
		sg_equality_of_races
	}
	
	on_start = {
		start_tutorial_lesson = { tutorial_lesson = egalitarian_society }
	}
}