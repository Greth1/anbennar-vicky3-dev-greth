﻿pmg_base_building_serpentbloom_farm = {
	production_methods = {
		pm_simple_farming
		pm_soil_enriching_farming
		pm_fertilization
		pm_chemical_fertilizer
	}
}

pmg_secondary_building_serpentbloom_farm = {
	production_methods = {
		pm_no_secondary
		pm_mushrooms
	}
}

pmg_harvesting_process_building_serpentbloom_farm = {
	production_methods = {
		pm_tools_disabled
		pm_tools
		pm_steam_threshers
		pm_tractors_building_rye_farm
		pm_compression_ignition_tractors_building_rye_farm
	}
}

pmg_ownership_land_building_serpentbloom_farm = {
	production_methods = {
		pm_privately_owned
		pm_publicly_traded
		pm_government_run
		pm_worker_cooperative_farm
	}
}