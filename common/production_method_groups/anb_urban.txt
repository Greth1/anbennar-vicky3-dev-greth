﻿pmg_base_building_mage_academy = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_basic_mage_academy
		pm_advanced_mage_academy
		pm_more_advanced_mage_academy
	}
}

pmg_damestear_mage_academy = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_damestear
		pm_damestear_mage_academy
	}
}

pmg_ownership_building_mage_academy = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_independent_mages
		pm_government_mages
		pm_religious_mages
	}
}
