﻿state_trait_venaans_tears = {
	icon = "gfx/interface/icons/state_trait_icons/waterfall.dds"
	
	modifier = {
		building_output_electricity_mult = 0.15
	}
}

state_trait_bloodgroves = {
	icon = "gfx/interface/icons/state_trait_icons/resources_lumber.dds"
	
	modifier = {
		building_output_hardwood_mult = 0.10
	}
}

state_trait_effelai = {
    icon = "gfx/interface/icons/state_trait_icons/swamp.dds"
	
	modifier = {
		building_output_hardwood_mult = 0.25
		state_construction_mult = -0.33
		state_infrastructure_mult = -0.33
        state_non_homeland_mortality_mult = 0.05
	}
}

state_trait_mushroom_forest = {
    icon = "gfx/interface/icons/state_trait_icons/good_soils.dds"
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = 0.2
	}
}


state_trait_ynn_river = {
	icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 40
	}
}

state_trait_ependar_mountains = {
    icon = "gfx/interface/icons/state_trait_icons/mountain.dds"
	
	modifier = {
		state_construction_mult = -0.2
		state_infrastructure_mult = -0.2
	}
}

state_trait_harafe_desert = {
    icon = "gfx/interface/icons/state_trait_icons/dry_climate.dds"
	
	modifier = {
		building_group_bg_agriculture_throughput_mult = -0.25
		state_construction_mult = -0.25
		state_infrastructure_mult = -0.25
	}
}

state_trait_lady_isobel = {
    icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 30
	}
}

state_trait_funashaweasin = {
    icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 30
	}
}

state_trait_harafroy = {
    icon = "gfx/interface/icons/state_trait_icons/river.dds"
	
	modifier = {
		state_infrastructure_add = 40
	}
}